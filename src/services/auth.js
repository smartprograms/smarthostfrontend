import decode from 'jwt-decode'
import { route } from 'preact-router'

const USER_KEY = 'smarthost_user'
const ACCESS_TOKEN_KEY = 'smarthost_auth'
const PRO_KEY = 'smarthost_pro'

export function login (token) {
	try {
		const decodedToken = decode(token)

		setAccessToken(token)
		setUser(JSON.stringify(decodedToken))

		if (isTokenExpired(token)) {
			console.log('token is expired?')
		} else {
			console.log('token is not expired!')
		}

		if (decodedToken.banned === true) route('/banned')
		else window.location.href = '/?from=login'
	} catch (error) {
		console.error(error)
		window.location.href = '/?auth=false&from=login'
	}
}

export function logout () {
	clearUser()
	clearAccessToken()

	window.location.href = '/?from=logout'
}

export function setSubscribeState (isPro) {
	localStorage.setItem(PRO_KEY, isPro)
}

export function getCurrentProStatus () {
	return new Promise((resolve) => {
		fetch(process.env.API_URL + '/payments/subscription/current', {
			headers: {
				Authorization: getAccessToken()
			}
		})
			.then((res) => {
				if (!res.ok) throw 'Error requesting'

				return res.json()
			})
			.then((response) => {
				if (!response) throw 'No response'

				const subscription = response.subscription

				if (subscription && (subscription.status === 'active' || subscription.status === 'trialing')) {
					resolve(true)
				} else {
					resolve(false)
				}
			})
			.catch((err) => {
				console.error(err)

				resolve(false)
			})
	})
}

export function getSubscribedState () {
	return localStorage.getItem(PRO_KEY) === 'true'
}

export function getUser () {
	return localStorage.getItem(USER_KEY)
}

export function getAccessToken () {
	return localStorage.getItem(ACCESS_TOKEN_KEY)
}

function clearUser () {
	localStorage.removeItem(USER_KEY)
}

function clearAccessToken () {
	localStorage.removeItem(ACCESS_TOKEN_KEY)
}

function setAccessToken (token) {
	localStorage.setItem(ACCESS_TOKEN_KEY, token)
}

export function setUser (user) {
	localStorage.setItem(USER_KEY, user)
}

export function isLoggedIn () {
	return !!getAccessToken() && !isTokenExpired()
}

function getTokenExpirationDate (encodedToken) {
	try {
		const token = decode(encodedToken)

		if (!token.exp) return null

		const date = new Date(0)
		date.setUTCSeconds(token.exp)

		return date
	} catch (error) {
		return null
	}
}

function isTokenExpired (token = getAccessToken()) {
	const expirationDate = getTokenExpirationDate(token)

	console.log('Token expiration date', expirationDate)

	return expirationDate < new Date()
}
