import { h, render } from 'preact'
import App from './components/App'
import './style/index.scss'

const node = document.getElementById('root')
render(<App />, node, node.lastChild)
