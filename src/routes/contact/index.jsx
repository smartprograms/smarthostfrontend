import { h, Component } from 'preact'
import { Page } from '../../components/ThemedComponents'

export default class Contact extends Component {
    render() {
        return (
            <Page
                className="all-center"
                style="max-width: 80vw; margin: 0 auto;"
            >
                <div className="row">
                    <div
                        className="col s12 l6"
                        style="padding: 0 50px; padding-bottom: 100px;"
                    >
                        <div className="heading">General Support</div>
                        <p className="description">
                            Have an issue with SmartHost? Would you like to
                            request a feature?
                        </p>
                        <br />
                        <p>
                            <a href="https://unsmart.co/smartprograms">
                                Join our discord{' '}
                            </a>
                        </p>
                        <br />
                        <p>
                            OR, Send us an email at{' '}
                            <a href="mailto:support@smartprograms.co">
                                support@smartprograms.co
                            </a>
                        </p>
                    </div>
                    <div className="col s12 l6" style="padding: 0 50px;">
                        <div className="heading">Payment Support</div>
                        <p className="description">
                            Have an issue with subscribing to SmartHost Pro?
                        </p>
                        <br />
                        <p>
                            Send us an email at{' '}
                            <a href="mailto:payments@smartprograms.co">
                                payments@smartprograms.co
                            </a>{' '}
                            and be sure to send us your Mixer Username, and if
                            applicable your Invoice ID.
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div
                        className="col s12"
                        style="padding: 0 50px; padding-bottom: 100px;"
                    >
                        <div className="heading">Business Contact</div>
                        <p className="description">
                            Are you a business looking for something specific
                            with SmartHost?
                        </p>
                        <br />
                        <p>
                            Send us an email at{' '}
                            <a href="mailto:garret@smartprograms.co">
                                garret@smartprograms.co
                            </a>{' '}
                            and we will be in contact with you!
                        </p>
                    </div>
                </div>
                <div className="row" style="margin: 0; padding-bottom: 10vh;">
                    <div className="col s12">
                        <div className="heading">Video Guide</div>
                        <div style="position: relative; height: 0; padding-bottom: 56.25%;">
                            <iframe
                                src="https://www.youtube-nocookie.com/embed/a8ZWTkruet0"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen
                                style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                            />
                        </div>
                    </div>
                </div>
            </Page>
        )
    }
}
