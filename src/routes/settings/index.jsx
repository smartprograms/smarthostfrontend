import { h, Component } from 'preact'
import { Link, route } from 'preact-router'
import {
	Page,
	Form,
	FormContent,
	Header,
	Heading,
	MainSettings,
	Setting,
	SorryButton,
	SorryContainer,
	SettingContent,
	RemoveButton,
	SortList,
	SortListItem,
	AddButton
} from './components'
import { SubHeading } from '../../components/ThemedComponents'
import { isLoggedIn, getAccessToken, getCurrentProStatus } from '../../services/auth'

import Error from '../error'

import './index.scss'

export default class Settings extends Component {
	constructor (props) {
		super(props)

		this.setState({
			loading: true
		})
	}

	componentDidMount () {
		if (!isLoggedIn()) {
			return this.setState({
				loading: false,
				error: {
					status: 401
				}
			})
		}

		const fetchSettings = () => {
			fetch(process.env.API_URL + '/users/current/settings', {
				headers: {
					Authorization: getAccessToken()
				},
				method: 'GET'
			})
				.then((response) => {
					if (response.status !== 200) {
						throw {
							status: response.status
						}
					} else {
						return response.json()
					}
				})
				.then((response) => {
					this.setState({
						loading: false,
						settings: response
					})
				})
				.catch((error) => {
					this.setState({
						loading: false,
						error: error
					})
				})
		}

		getCurrentProStatus().then((pro) => {
			this.setState({ pro })

			const failedSettings = localStorage.getItem('smarthost_settings_fromFail')
			if (failedSettings) {
				try {
					this.setState({
						loading: false,
						settings: JSON.parse(failedSettings)
					})

					M.toast({
						html:
							'Looks like your last settings did not save correctly, no worries we auto filled these back in for you to try again!',
						displayLength: 10000,
						classes: 'green'
					})
				} catch (error) {
					fetchSettings()
				} finally {
					localStorage.removeItem('smarthost_settings_fromFail')
				}
			} else fetchSettings()
		})
	}

	componentDidUpdate () {
		if (this.state.settings && this.state.fullLoad !== true) {
			this.state.fullLoad = true

			this.state.settings.hostCategory = this.state.settings.hostCategory.filter((el) => el.key !== 'PARTNERS')

			const hostActive = document.querySelector('#HOST-ACTIVE')
			if (hostActive && !hostActive.checked) hostActive.click()

			const stylesSort = document.querySelector('.styles-sort')
			if (stylesSort) new Sortable(stylesSort, { animation: 150 })

			const sortChannels = document.querySelector('.channels-sort')
			if (sortChannels) new Sortable(sortChannels, { animation: 150 })

			const sortPriorityChannels = document.querySelector('.channels-sort-priority')
			if (sortPriorityChannels) new Sortable(sortPriorityChannels, { animation: 150 })

			const sortTeams = document.querySelector('.teams-sort')
			if (sortTeams) new Sortable(sortTeams, { animation: 150 })

			const sortGames = document.querySelector('.games-sort')
			if (sortGames) new Sortable(sortGames, { animation: 150 })

			const randomChannelOrder = document.querySelector('#RANDOM-ORDER-CHANNELS')
			if (randomChannelOrder && randomChannelOrder.checked !== this.state.settings.randomChannelOrder)
				randomChannelOrder.click()

			const randomTeamOrder = document.querySelector('#RANDOM-ORDER-TEAMS')
			if (randomTeamOrder && randomTeamOrder.checked !== this.state.settings.randomTeamOrder) randomTeamOrder.click()

			this.state.settings.hostCategory.forEach((category) => {
				const categoryElem = document.querySelector('#' + category.key)
				if (categoryElem && !categoryElem.checked) categoryElem.click()
			})

			if (this.state.settings.hostMessaging.sendMessage && this.state.settings.hostMessaging.message !== '') {
				const messageCheckbox = document.querySelector('#HOST-MESSAGE')
				// const whisperCheckbox = document.querySelector('#HOST-MESSAGE-WHISPER')
				const messageElem = document.querySelector('#host-message-start-val')
				const messageLabel = document.querySelector('[for="host-message-start-val"]')

				if (messageElem && messageLabel && messageCheckbox) {
					if (!messageElem.value)
						messageElem.value = this.state.settings.hostMessaging.message.replace(
							'(hosting provided by smarthostapp)',
							''
						)
					if (!messageElem.classList.contains('valid')) messageElem.classList += ' valid'
					if (!messageLabel.classList.contains('active')) messageLabel.classList += ' active'
					if (!messageCheckbox.checked) messageCheckbox.click()
					// if (whisperCheckbox.checked !== this.state.settings.hostMessaging.shouldWhisper) whisperCheckbox.click()
				}
			}

			if (this.state.settings.hostMessaging.sendEndMessage && this.state.settings.hostMessaging.endMessage !== '') {
				const messageEndCheckbox = document.querySelector('#HOST-MESSAGE-END')
				const messageEndElem = document.querySelector('#host-message-end-val')
				const messageEndLabel = document.querySelector('[for="host-message-end-val"]')

				if (messageEndElem && messageEndLabel && messageEndCheckbox) {
					if (!messageEndElem.value)
						messageEndElem.value = this.state.settings.hostMessaging.endMessage.replace(
							'(hosting provided by smarthostapp)',
							''
						)
					if (!messageEndElem.classList.contains('valid')) messageEndElem.classList += ' valid'
					if (!messageEndLabel.classList.contains('active')) messageEndLabel.classList += ' active'
					if (!messageEndCheckbox.checked) messageEndCheckbox.click()
				}
			}

			if (this.state.settings.hostChanging.maxRating) {
				const ratingElem = document.querySelector('#max-rating')
				if (ratingElem) {
					ratingElem.value = this.state.settings.hostChanging.maxRating

					M.FormSelect.init(ratingElem)
				}
			}

			if (this.state.settings.hostChanging.rehostTimeout !== 0) {
				const rehostElem = document.querySelector('#REHOST-TIMEOUT')
				const rehostInput = document.querySelector('#rehost-timeout-val')
				const rehostLabel = document.querySelector('[for="rehost-timeout-val"]')

				if (rehostElem && rehostLabel && rehostInput) {
					if (!rehostInput.value) rehostInput.value = this.state.settings.hostChanging.rehostTimeout
					if (!rehostInput.classList.contains('valid')) rehostInput.classList += ' valid'
					if (!rehostLabel.classList.contains('active')) rehostLabel.classList += ' active'
					if (!rehostElem.checked) rehostElem.click()

					const rehostManualElem = document.querySelector('#manual-rehost')

					if (rehostManualElem) rehostManualElem.checked = this.state.settings.hostChanging.rehostManual
				}
			}

			if (this.state.settings.hostChanging.hostAfterOfflineTimeout !== 0) {
				const offlineElem = document.querySelector('#OFFLINE-TIMEOUT')
				const offlineInput = document.querySelector('#offline-timeout-val')
				const offlineLabel = document.querySelector('[for="offline-timeout-val"]')

				if (offlineElem && offlineInput && offlineLabel) {
					if (!offlineInput.value) offlineInput.value = this.state.settings.hostChanging.hostAfterOfflineTimeout
					if (!offlineInput.classList.contains('valid')) offlineInput.classList += ' valid'
					if (!offlineLabel.classList.contains('active')) offlineLabel.classList += ' active'
					if (!offlineElem.checked) offlineElem.click()
				}
			}

			if (this.state.settings.hostChanging.useGamesList) {
				const gameListElem = document.querySelector('#SPECIFIC-GAMES')

				if (!gameListElem.checked) gameListElem.click()
			}

			M.AutoInit()

			this.eventListener()
		}
	}

	eventListener () {
		document.querySelector('#add-channel').addEventListener('keydown', (event) => {
			const keyCode = event.which || event.keyCode
			if (keyCode === 13) document.querySelector('.btn-add-channel').click()
		})
		document.querySelector('#add-channel-priority').addEventListener('keydown', (event) => {
			const keyCode = event.which || event.keyCode
			if (keyCode === 13) document.querySelector('.btn-add-channel-priority').click()
		})

		document.querySelector('#add-channel-exclusion').addEventListener('keydown', (event) => {
			const keyCode = event.which || event.keyCode
			if (keyCode === 13) document.querySelector('.btn-add-channel-exclusion').click()
		})

		document.querySelector('#add-team').addEventListener('keydown', (event) => {
			const keyCode = event.which || event.keyCode
			if (keyCode === 13) document.querySelector('.btn-add-team').click()
		})

		document.querySelector('#game-name').addEventListener('keydown', (event) => {
			const keyCode = event.which || event.keyCode
			if (keyCode === 13) document.querySelector('.btn-add-game').click()
		})
	}

	showHideLists (e) {
		const checkbox = e.target || e

		const id = checkbox.getAttribute('id')

		if (
			id === 'HOST-MESSAGE' ||
			id === 'HOST-MESSAGE-END' ||
			id === 'TEAMLIST' ||
			id === 'CHANNELLIST' ||
			id === 'HIGHPRIORITYCHANNELLIST' ||
			id === 'SPECIFIC-GAMES' ||
			id === 'REHOST-TIMEOUT' ||
			id === 'OFFLINE-TIMEOUT'
		) {
			const elem = document.getElementsByClassName(id)[0]
			if (elem) elem.style.display = checkbox.checked ? 'block' : 'none'

			if (id === 'HOST-MESSAGE') this.state.settings.hostMessaging.sendMessage = checkbox.checked
			else if (id === 'HOST-MESSAGE-END') this.state.settings.hostMessaging.sendEndMessage = checkbox.checked
			else if (id === 'SPECIFIC-GAMES') this.state.settings.hostChanging.useGamesList = checkbox.checked
			else if (id === 'REHOST-TIMEOUT' && checkbox.checked === false) this.state.settings.hostChanging.rehostTimeout = 0
			else if (id === 'OFFLINE-TIMEOUT' && checkbox.checked === false)
				this.state.settings.hostChanging.hostAfterOfflineTimeout = 0
		} else if (id === 'HOST-ACTIVE') {
			this.state.settings.active = checkbox.checked

			const elem = document.getElementsByClassName('all-settings')[0]
			if (elem) elem.style.display = checkbox.checked ? 'block' : 'none'

			const sorryElem = document.querySelector('#sorry-message')
			if (sorryElem) sorryElem.style.display = checkbox.checked ? 'none' : 'block'
		}
	}

	sortHostPriorities () {
		const catsList = document.querySelector('.styles-sort')

		if (catsList) {
			const items = catsList.getElementsByTagName('li')

			for (let i = 0; i < items.length; ++i) {
				const id = items[i].getAttribute('data')
				const catIndex = this.state.settings.hostCategory.findIndex((cat) => cat.key === id)

				if (catIndex > -1) this.state.settings.hostCategory[catIndex].priority = i
			}
		}

		this.state.settings.hostCategory.sort((a, b) => a.priority - b.priority)
	}

	addChannelExclusion () {
		if (this.state.addingChannelExclusion === true) return
		this.state.addingChannelExclusion = true

		if (!this.state.settings.channelExclusion) this.state.settings.channelExclusion = []

		const channelElem = document.querySelector('#add-channel-exclusion')
		try {
			const channelName = channelElem ? channelElem.value : ''

			const channelAlreadyExists =
				this.state.settings.channelExclusion.find(
					(channel) => channel.token.toLowerCase() === channelName.toLowerCase()
				) !== undefined
			if (channelName.length === 0 || channelAlreadyExists) throw 'Channel is already in the list or not valid'

			fetch('https://mixer.com/api/v1/channels/' + channelName, {
				method: 'GET'
			})
				.then((r) => {
					if (!r) throw 'Your browser ran into an issue'
					if (r.status !== 200) {
						if (r.status === 404) throw 'Channel does not exist'
						else throw 'Mixer returned an error'
					} else return r.json()
				})
				.then((data) => {
					this.state.settings.channelExclusion.push({
						token: data.token,
						channelid: data.id
					})

					channelElem.value = ''
					this.state.addingChannelExclusion = false

					this.forceUpdate()
				})
				.catch((error) => {
					error
						.json()
						.then((res) => {
							M.toast({ html: 'Mixer Error: ' + res.message, displayLength: 10000, classes: 'red accent-4' })
						})
						.catch((err) => {
							M.toast({ html: 'An unknown error occurred', displayLength: 10000, classes: 'red accent-4' })
						})

					channelElem.value = ''
					this.state.addingChannelExclusion = false
				})
		} catch (error) {
			M.toast({ html: 'Error: ' + error, displayLength: 10000, classes: 'red accent-4' })

			channelElem.value = ''
			this.state.addingChannelExclusion = false
		}
	}

	removeChannelExclusion (e) {
		const channelElem = e.target.parentNode

		const id = channelElem.getAttribute('data')
		const findIndex = this.state.settings.channelExclusion.findIndex((channel) => channel.channelid === parseInt(id))

		if (findIndex > -1) {
			this.state.settings.channelExclusion.splice(findIndex, 1)
			this.forceUpdate()
		}
	}

	addChannel (priority = false) {
		if (this.state.addingChannel === true) return
		this.state.addingChannel = true

		const channelElem = priority
			? document.querySelector('#add-channel-priority')
			: document.querySelector('#add-channel')
		try {
			if (!this.state.settings.highPriorityChannelList) this.state.settings.highPriorityChannelList = []
			if (!this.state.settings.channelList) this.state.settings.channelList = []

			const list = priority ? this.state.settings.highPriorityChannelList : this.state.settings.channelList

			if (this.state.pro === false && list.length >= 100)
				throw 'You are not a PRO Subscriber, you cannot have more than 100 channels in the list'

			const channelName = channelElem ? channelElem.value : ''

			const channelAlreadyExists =
				list.find((channel) => channel.token.toLowerCase() === channelName.toLowerCase()) !== undefined
			if (channelName.length === 0 || channelAlreadyExists) throw 'Channel is already in the list or not valid'

			this.sortChannels()

			fetch('https://mixer.com/api/v1/channels/' + channelName, {
				method: 'GET'
			})
				.then((r) => {
					if (!r) throw 'Your browser ran into an issue'
					if (r.status !== 200) {
						if (r.status === 404) throw 'Channel does not exist'
						else throw 'Mixer returned an error'
					} else return r.json()
				})
				.then((data) => {
					if (data.preferences['hosting:allow'] === false) throw data.token + ' does not allow hosting.'

					if (priority) {
						list.push({
							token: data.token,
							channelid: data.id,
							priority: list.length
						})
					} else {
						list.push({
							token: data.token,
							channelid: data.id,
							priority: list.length
						})
					}

					channelElem.value = ''
					this.state.addingChannel = false

					this.forceUpdate()
				})
				.catch((error) => {
					M.toast({ html: 'Error: ' + error, displayLength: 10000, classes: 'red accent-4' })

					channelElem.value = ''
					this.state.addingChannel = false
				})
		} catch (error) {
			M.toast({ html: error, displayLength: 10000, classes: 'red accent-4' })

			channelElem.value = ''
			this.state.addingChannel = false
		}
	}

	removeChannel (priority, e) {
		const channelElem = e.target.parentNode

		const id = channelElem.getAttribute('data')

		const list = priority ? this.state.settings.highPriorityChannelList : this.state.settings.channelList

		const findIndex = list.findIndex((channel) => channel.channelid === parseInt(id))

		if (findIndex > -1) {
			list.splice(findIndex, 1)
			this.forceUpdate()
		}
	}
	sortChannels () {
		const channelsList = document.querySelector('.channels-sort')

		if (channelsList) {
			const items = channelsList.getElementsByTagName('li')

			let channelIds = []
			for (var i = 0; i < items.length; ++i) {
				const id = parseInt(items[i].getAttribute('data'))
				const channelIndex = this.state.settings.channelList.findIndex((channel) => channel.channelid === id)

				if (channelIds.includes(id) && channelIndex > -1) {
					this.state.settings.channelList.splice(channelIndex, 1)
				} else if (channelIndex > -1) {
					channelIds.push(id)
					this.state.settings.channelList[channelIndex].priority = i
				}
			}
		}

		if (this.state.settings.channelList) this.state.settings.channelList.sort((a, b) => a.priority - b.priority)

		const priorityChannelsList = document.querySelector('.channels-sort-priority')

		if (priorityChannelsList) {
			const items = priorityChannelsList.getElementsByTagName('li')

			let channelIds = []
			for (var i = 0; i < items.length; ++i) {
				const id = parseInt(items[i].getAttribute('data'))
				const channelIndex = this.state.settings.highPriorityChannelList.findIndex(
					(channel) => channel.channelid === id
				)

				if (channelIds.includes(id) && channelIndex > -1) {
					this.state.settings.highPriorityChannelList.splice(channelIndex, 1)
				} else if (channelIndex > -1) {
					channelIds.push(id)
					this.state.settings.highPriorityChannelList[channelIndex].priority = i
				}
			}
		}

		if (this.state.settings.highPriorityChannelList)
			this.state.settings.highPriorityChannelList.sort((a, b) => a.priority - b.priority)
	}

	addTeam () {
		if (this.state.addingTeam === true) return
		this.state.addingTeam = true

		const teamElem = document.querySelector('#add-team')
		try {
			if (this.state.pro === false && this.state.settings.teamList.length >= 3)
				throw 'You are not a PRO Subscriber, you cannot have more than 3 teams in the list'

			const teamName = teamElem ? teamElem.value : ''
			const teamRegex = /^[a-z0-9_-]+$/i

			const teamAlreadyExists =
				this.state.settings.teamList.find((team) => team.name.toLowerCase() === teamName.toLowerCase()) !== undefined
			if (teamName.length < 4 || teamName.length > 20 || !teamName.match(teamRegex) || teamAlreadyExists)
				throw 'Team is already in the list or not valid'

			this.sortTeams()

			fetch('https://mixer.com/api/v1/teams/' + teamName, {
				method: 'GET'
			})
				.then((r) => {
					if (!r) throw 'Your browser ran into an issue'
					if (r.status !== 200) {
						if (r.status === 404) throw 'Channel does not exist'
						else throw 'Mixer returned an error'
					} else return r.json()
				})
				.then((data) => {
					const teamAlreadyExists = this.state.settings.teamList.find((team) => team.id === data.id) !== undefined
					if (teamAlreadyExists) throw 'Team is already in the list'

					this.state.settings.teamList.push({
						name: data.token,
						id: data.id,
						priority: this.state.settings.teamList.length
					})

					teamElem.value = ''
					this.state.addingTeam = false

					this.forceUpdate()
				})
				.catch((error) => {
					M.toast({ html: error, displayLength: 10000, classes: 'red accent-4' })

					teamElem.value = ''
					this.state.addingTeam = false
				})
		} catch (error) {
			M.toast({ html: 'Error: ' + error, displayLength: 10000, classes: 'red accent-4' })

			teamElem.value = ''
			this.state.addingTeam = false
		}
	}
	removeTeam (e) {
		const teamElem = e.target.parentNode

		const id = teamElem.getAttribute('data')
		const findIndex = this.state.settings.teamList.findIndex((team) => team.id === parseInt(id))

		if (findIndex > -1) {
			this.state.settings.teamList.splice(findIndex, 1)
			this.forceUpdate()
		}
	}
	sortTeams () {
		const teamsList = document.querySelector('.teams-sort')

		if (teamsList) {
			const items = teamsList.getElementsByTagName('li')

			let teamIds = []
			for (var i = 0; i < items.length; ++i) {
				const id = parseInt(items[i].getAttribute('data'))
				const teamIndex = this.state.settings.teamList.findIndex((team) => team.id === id)

				if (teamIds.includes(id) && teamIndex > -1) {
					this.state.settings.teamList.splice(teamIndex, 1)
				} else if (teamIndex > -1) {
					teamIds.push(id)
					this.state.settings.teamList[teamIndex].priority = i
				}
			}
		}

		this.state.settings.teamList.sort((a, b) => a.priority - b.priority)
	}

	styleChange (e) {
		try {
			const checkbox = e.target

			const id = checkbox.getAttribute('id')
			this.sortHostPriorities()

			if (checkbox.checked) {
				if (this.state.settings.hostCategory.findIndex((cat) => cat.key === id) > -1) return

				if (this.state.pro === false && this.state.settings.hostCategory.length >= 1) {
					checkbox.checked = false

					throw 'You are not a PRO Subscriber, you cannot have more than 1 host style at a time'
				}

				this.state.settings.hostCategory.push({
					name: checkbox.getAttribute('alt'),
					key: id,
					priority: this.state.settings.hostCategory.length
				})
			} else {
				const findIndex = this.state.settings.hostCategory.findIndex((cat) => cat.key === id)

				if (findIndex > -1) this.state.settings.hostCategory.splice(findIndex, 1)
			}

			this.forceUpdate()
		} catch (error) {
			M.toast({ html: error, displayLength: 10000, classes: 'red accent-4' })
		}
	}

	removeStyle (e) {
		const styleElem = e.target.parentNode

		const id = styleElem.getAttribute('data')

		const checkbox = document.querySelector('#' + id)

		if (checkbox) checkbox.click()
	}

	changeAndUpdate (e) {
		this.styleChange(e)
		this.showHideLists(e)
	}

	maxRatingChange (e) {
		const value = e.target.options[e.target.selectedIndex].value

		this.state.settings.hostChanging.maxRating = value
	}
	rehostValueChange (e) {
		this.state.settings.hostChanging.rehostTimeout = parseInt(e.target.value)
	}
	rehostManualChecked (e) {
		this.state.settings.hostChanging.rehostManual = e.target.checked
	}
	offlineValueChange (e) {
		this.state.settings.hostChanging.hostAfterOfflineTimeout = parseInt(e.target.value)
	}
	messageValueChange (e) {
		this.state.settings.hostMessaging.message = e.target.value.replace(/ +/g, ' ')

		const linkCheck = this.state.settings.hostMessaging.message.replace(
			/(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?[^ ]+/gi,
			''
		)

		if (linkCheck !== this.state.settings.hostMessaging.message)
			M.toast({
				html: 'You cannot put links in the host message!',
				displayLength: 10000,
				classes: 'yellow'
			})

		this.state.settings.hostMessaging.message = linkCheck
		e.target.value = linkCheck
	}
	messageEndValueChange (e) {
		this.state.settings.hostMessaging.endMessage = e.target.value.replace(/ +/g, ' ')

		const linkCheck = this.state.settings.hostMessaging.endMessage.replace(
			/(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?[^ ]+/gi,
			''
		)

		if (linkCheck !== this.state.settings.hostMessaging.endMessage)
			M.toast({
				html: 'You cannot put links in the host message!',
				displayLength: 10000,
				classes: 'yellow'
			})

		this.state.settings.hostMessaging.endMessage = linkCheck
		e.target.value = linkCheck
	}
	messageWhisperChecked (e) {
		let checked = true

		if (this.state.pro === true) checked = e.target.checked
		else if (!e.target.checked)
			M.toast({
				html: 'You must be a pro member to send messages as a public message!',
				displayLength: 10000,
				classes: 'red'
			})

		e.target.checked = checked
		this.state.settings.hostMessaging.shouldWhisper = checked
	}

	gamesListChecked (e) {
		this.state.settings.hostChanging.useGamesList = e.target.checked
	}

	addGame () {
		if (this.state.addingGame === true) return
		this.state.addingGame = true

		const gameElem = document.querySelector('#game-name')

		try {
			const gameName = gameElem ? gameElem.value : ''

			const gameAlreadyExists =
				this.state.settings.hostChanging.hostGames.find(
					(game) => game.name.toLowerCase() === gameName.toLowerCase()
				) !== undefined
			if (gameName.length === 0 || gameAlreadyExists) throw 'Game is already in the list or not valid'

			this.sortChannels()

			const queryForGame = () => {
				fetch('https://mixer.com/api/v1/types?query=' + gameName, {
					method: 'GET'
				})
					.then((r) => {
						if (!r) throw 'Your browser ran into an issue'
						if (r.status !== 200) {
							if (r.status === 404) throw 'Channel does not exist'
							else throw 'Mixer returned an error'
						} else return r.json()
					})
					.then((data) => {
						if (data.length === 0) throw 'No Games Match the name of ' + gameName

						data = data[0]

						this.state.settings.hostChanging.hostGames.push({
							name: data.name,
							typeid: data.id
						})

						gameElem.value = ''
						this.state.addingGame = false

						this.forceUpdate()
					})
					.catch((error) => {
						M.toast({ html: 'Error: ' + error, displayLength: 10000, classes: 'red accent-4' })

						gameElem.value = ''
						this.state.addingGame = false
					})
			}

			const getExact = () => {
				fetch('https://mixer.com/api/v1/types?where=name:eq:' + gameName, {
					method: 'GET'
				})
					.then((r) => {
						if (r.status !== 200) throw r
						else return r.json()
					})
					.then((data) => {
						if (data.length === 0) throw 'No games'

						data = data[0]

						this.state.settings.hostChanging.hostGames.push({
							name: data.name,
							typeid: data.id
						})

						gameElem.value = ''
						this.state.addingGame = false

						this.forceUpdate()
					})
					.catch((error) => {
						queryForGame()
					})
			}

			getExact()
		} catch (error) {
			M.toast({ html: error, displayLength: 10000, classes: 'red accent-4' })

			gameElem.value = ''
			this.state.addingGame = false
		}
	}
	removeGame (e) {
		const gameElem = e.target.parentNode
		const id = gameElem.getAttribute('data')

		const findIndex = this.state.settings.hostChanging.hostGames.findIndex((game) => game.typeid === parseInt(id))

		if (findIndex > -1) {
			this.state.settings.hostChanging.hostGames.splice(findIndex, 1)
			this.forceUpdate()
		}
	}

	randomOrderChecked (e) {
		if (e.target.getAttribute('id').includes('TEAM')) this.state.settings.randomTeamOrder = e.target.checked
		else this.state.settings.randomChannelOrder = e.target.checked

		console.log(this.state.settings)
	}

	submitForm () {
		try {
			if (this.state.saving) throw 'Already attempting to save settings.'
			this.setState({ saving: true })

			if (this.state.settings.active) {
				if (this.state.settings.hostCategory.length === 0)
					throw 'Hosting is set to active but you did not select any host styles.'
				if (
					(this.state.settings.hostMessaging.sendMessage && this.state.settings.hostMessaging.message === '') ||
					(this.state.settings.hostMessaging.sendEndMessage && this.state.settings.hostMessaging.endMessage === '')
				)
					throw 'You enabled host messaging but did not set a message.'
				if (
					this.state.settings.hostCategory.findIndex((cat) => cat.key === 'CHANNELLIST') > -1 &&
					this.state.settings.channelList.length === 0
				)
					throw 'You enabled the channels list host style but did not add any channels to the list.'
				if (
					this.state.settings.hostCategory.findIndex((cat) => cat.key === 'HIGHPRIORITYCHANNELLIST') > -1 &&
					this.state.settings.highPriorityChannelList.length === 0
				)
					throw 'You enabled the high priority channels list host style but did not add any channels to the list.'
				if (
					this.state.settings.hostCategory.findIndex((cat) => cat.key === 'TEAMLIST') > -1 &&
					this.state.settings.teamList.length === 0
				)
					throw 'You enabled the teams list host style but did not add any team to the list.'

				if (this.state.settings.hostChanging.useGamesList && this.state.settings.hostChanging.hostGames.length === 0)
					throw 'You enabled the host games list but did not add any games to the list.'
			}

			this.sortHostPriorities()
			this.sortChannels()
			this.sortTeams()

			fetch(process.env.API_URL + '/users/current/settings', {
				method: 'POST',
				body: JSON.stringify({
					active: true,
					channelid: this.state.settings.channelid,
					channelExclusion: this.state.settings.channelExclusion,
					channelList: this.state.settings.channelList,
					highPriorityChannelList: this.state.settings.highPriorityChannelList,
					teamList: this.state.settings.teamList,
					randomChannelOrder: this.state.settings.randomChannelOrder,
					randomTeamOrder: this.state.settings.randomTeamOrder,
					hostMessaging: this.state.settings.hostMessaging,
					hostChanging: this.state.settings.hostChanging,
					hostCategory: this.state.settings.hostCategory
				}),
				headers: {
					Authorization: getAccessToken(),
					'Content-Type': 'application/json'
				}
			})
				.then((res) => {
					if (!res) throw 'Your browser ran into an issue'
					if (res.status !== 200) {
						if (res.status === 403 || res.status === 400) {
							res
								.json()
								.then((r) => {
									M.toast({
										html: r.message,
										displayLength: 10000,
										classes: 'red'
									})
								})
								.catch(() => {
									M.toast({
										html:
											res.statusText === 403
												? 'Oops, you are not a sub you cannot use these settings.'
												: 'Our servers are having trouble processing this.',
										displayLength: 10000,
										classes: 'red'
									})
								})
								.finally(() => this.setState({ saving: false }))
						} else throw 'Our servers are having trouble processing this.'
					} else route('/settings/success')
				})
				.catch((error) => {
					console.error('-- UPDATE SETTINGS ERROR --')
					console.error(error)

					localStorage.setItem('smarthost_settings_fromFail', JSON.stringify(this.state.settings))
					route('/settings/failure')
				})
		} catch (error) {
			M.toast({ html: error, displayLength: 10000, classes: 'red accent-4' })

			this.setState({ saving: false })
		}
	}

	leaveSmartHost () {
		fetch(process.env.API_URL + '/users/current/deactivate', {
			method: 'POST',
			headers: {
				Authorization: getAccessToken(),
				'Content-Type': 'application/json'
			}
		})
			.then((res) => {
				if (!res.ok) throw res

				route('/settings/leave')
			})
			.catch(() => route('/settings/failure'))
	}

	render () {
		if (typeof this.state.loading === 'boolean' && this.state.loading === false)
			if (this.state.error !== undefined)
				return (
					<Error
						status={this.state.error.status || 500}
						message={this.state.error.message || 'An unknown error has occurred'}
						from="settings"
					/>
				)
			else
				return (
					<Page>
						<Form className="settings-form col s12 z-depth-4">
							<Header className="header">
								<Heading>Host Settings</Heading>
							</Header>

							<FormContent className="form">
								<Setting className="active-host pb-30">
									<label>
										<input type="checkbox" id="HOST-ACTIVE" onClick={this.showHideLists.bind(this)} />
										<span>Activate Hosting?</span>
									</label>
								</Setting>
								<SorryContainer id="sorry-message" className="container pb-30">
									<p>
										Is there an issue with SmartHost? Be sure to <Link href="/contact">contact us</Link> so we can
										resolve your issues, we would hate to see you go!
									</p>

									<SorryButton className="btn" onClick={this.leaveSmartHost.bind(this)}>
										Leave SmartHost :(
									</SorryButton>
								</SorryContainer>

								<MainSettings className="all-settings">
									<Setting className="host-styles">
										<SubHeading>Hosting Styles</SubHeading>

										<SettingContent className="styles">
											{this.state.pro ? (
												<label>
													<input
														type="checkbox"
														id="HIGHPRIORITYCHANNELLIST"
														alt="Priority Channels List"
														onClick={this.changeAndUpdate.bind(this)}
													/>
													<span>High Priority Channels List</span>
												</label>
											) : (
												<div style="position: absolute;" />
											)}
											<label>
												<input
													type="checkbox"
													id="CHANNELLIST"
													alt="Channels List"
													onClick={this.changeAndUpdate.bind(this)}
												/>
												<span>Channels List</span>
											</label>
											<label>
												<input
													type="checkbox"
													id="TEAMLIST"
													alt="Teams List"
													onClick={this.changeAndUpdate.bind(this)}
												/>
												<span>Teams List</span>
											</label>
											<label>
												<input
													type="checkbox"
													id="FOLLOWEDPARTNERS"
													alt="Followed Partners"
													onClick={this.styleChange.bind(this)}
												/>
												<span>Followed Partners</span>
											</label>
											<label>
												<input
													type="checkbox"
													id="FOLLOWING"
													alt="Followed Users"
													onClick={this.styleChange.bind(this)}
												/>
												<span>Followed Users</span>
											</label>
										</SettingContent>

										<SettingContent>
											<List
												class="styles-sort"
												data="key"
												items={this.state.settings.hostCategory || []}
												property="name"
												removeClick={this.removeStyle.bind(this)}
											/>
											{this.state.settings.hostCategory && this.state.settings.hostCategory.length > 0 ? (
												<div />
											) : (
												<div>
													Start by checking some of the host categories above, if you have any questions about them feel
													free to <Link href="/contact">contact us!</Link>
												</div>
											)}
										</SettingContent>
									</Setting>

									<Setting className="HIGHPRIORITYCHANNELLIST">
										<SubHeading>High Priority Channels List</SubHeading>
										<SettingContent>
											<List
												class="channels-sort-priority"
												data="channelid"
												items={this.state.settings.highPriorityChannelList || []}
												property="token"
												removeClick={this.removeChannel.bind(this, true)}
											/>
											{!this.state.settings.highPriorityChannelList ||
											this.state.settings.highPriorityChannelList.length === 0 ? (
												<div>Start adding channels to the list just enter the channel name below!</div>
											) : (
												<div />
											)}

											<div class="row pt-30">
												<div class="input-field col s12 m8">
													<input id="add-channel-priority" type="text" />
													<label for="add-channel-priority">Channel Name</label>
												</div>
												<div class="input-field col s12 m3">
													<AddButton
														className="btn waves-effect waves-light btn-add-channel-priority light-blue"
														onClick={this.addChannel.bind(this, true)}
													>
														Add Channel
													</AddButton>
												</div>
											</div>
										</SettingContent>
									</Setting>

									<Setting className="CHANNELLIST">
										<SubHeading>Channels List</SubHeading>
										<SettingContent>
											<List
												class="channels-sort"
												data="channelid"
												items={this.state.settings.channelList || []}
												property="token"
												removeClick={this.removeChannel.bind(this, false)}
											/>
											{this.state.settings.channelList && this.state.settings.channelList.length > 0 ? (
												<div>
													<label>
														<input
															type="checkbox"
															id="RANDOM-ORDER-CHANNELS"
															onClick={this.randomOrderChecked.bind(this)}
														/>
														<span>Random Order?</span>
													</label>
												</div>
											) : (
												<div>Start adding channels to the list just enter the channel name below!</div>
											)}

											<div class="row pt-30">
												<div class="input-field col s12 m8">
													<input id="add-channel" type="text" />
													<label for="add-channel">Channel Name</label>
												</div>
												<div class="input-field col s12 m3">
													<AddButton
														className="btn waves-effect waves-light btn-add-channel light-blue"
														onClick={this.addChannel.bind(this, false)}
													>
														Add Channel
													</AddButton>
												</div>
											</div>
										</SettingContent>
									</Setting>

									<Setting className="TEAMLIST">
										<SubHeading>Teams List</SubHeading>
										<SettingContent>
											<List
												class="teams-sort"
												data="id"
												items={this.state.settings.teamList || []}
												property="name"
												removeClick={this.removeTeam.bind(this)}
											/>
											{this.state.settings.teamList && this.state.settings.teamList.length > 0 ? (
												<div>
													<label>
														<input
															type="checkbox"
															id="RANDOM-ORDER-TEAMS"
															onClick={this.randomOrderChecked.bind(this)}
														/>
														<span>Random Order?</span>
													</label>
												</div>
											) : (
												<div>Start adding teams to the list just enter the team name below!</div>
											)}

											<div class="row pt-30">
												<div class="input-field col s12 m8">
													<input id="add-team" type="text" />
													<label for="add-team">Team Name</label>
												</div>
												<div class="input-field col s12 m3">
													<AddButton
														className="btn waves-effect waves-light btn-add-team light-blue"
														onClick={this.addTeam.bind(this)}
													>
														Add Team
													</AddButton>
												</div>
											</div>
										</SettingContent>
									</Setting>

									<Setting className="host-messaging">
										<SubHeading>Host Messaging</SubHeading>
										<br />
										<small>%user% = username of person hosted</small>

										<SettingContent>
											<label>
												<input type="checkbox" id="HOST-MESSAGE" onClick={this.showHideLists.bind(this)} />
												<span>Send Host Message</span>
											</label>

											<div className="HOST-MESSAGE row pt-30">
												<div class="input-field col s12 m8">
													<input
														id="host-message-start-val"
														type="text"
														class="validate"
														onChange={this.messageValueChange.bind(this)}
														maxLength="160"
													/>
													<label for="host-message-start-val">Message</label>
												</div>
												<div className="col s12">
													{this.state.pro === false && (
														<div>
															<p>
																You are not a PRO Subscriber therefore a small promo will appear in all of your
																messages.
															</p>
															<p>*Promo message: "(hosting provided by smarthostapp)"</p>
														</div>
													)}
												</div>
											</div>
										</SettingContent>
									</Setting>

									<Setting className="host-games">
										<SubHeading>Host Games</SubHeading>
										<SettingContent>
											<label>
												<input type="checkbox" id="SPECIFIC-GAMES" onClick={this.showHideLists.bind(this)} />
												<span>Host Only If Playing Specific Game(s)</span>{' '}
												<small>NOW OUT! (If you have issues with this be sure to message us!)</small>
											</label>

											<div className="SPECIFIC-GAMES pt-30">
												<List
													class="games-sort"
													data="typeid"
													items={this.state.settings.hostChanging.hostGames || []}
													property="name"
													removeClick={this.removeGame.bind(this)}
												/>
												{this.state.settings.hostChanging.hostGames &&
												this.state.settings.hostChanging.hostGames.length > 0 ? (
													<div />
												) : (
													<div>Start adding games to the list just enter the game name below!</div>
												)}

												<div class="row pt-30">
													<div class="input-field col s12 m8">
														<input id="game-name" type="text" />
														<label for="game-name">Game Name</label>
													</div>
													<div class="input-field col s12 m3">
														<AddButton
															className="btn waves-effect waves-light btn-add-game light-blue"
															onClick={this.addGame.bind(this)}
														>
															Add Game
														</AddButton>
													</div>
												</div>
											</div>
										</SettingContent>
									</Setting>

									<Setting className="CHANNELEXCLUSION">
										<SubHeading>Channels Exclusion List</SubHeading>
										<SettingContent>
											<List
												class="channels-exclusion"
												data="channelid"
												items={this.state.settings.channelExclusion || []}
												property="token"
												removeClick={this.removeChannelExclusion.bind(this)}
											/>
											{this.state.settings.channelExclusion && this.state.settings.channelExclusion.length > 0 ? (
												<div />
											) : (
												<div>Start adding channels to the exclusion list just enter the channel name below!</div>
											)}

											<div class="row pt-30">
												<div class="input-field col s12 m8">
													<input id="add-channel-exclusion" type="text" />
													<label for="add-channel-exclusion">Channel Name</label>
												</div>
												<div class="input-field col s12 m3">
													<AddButton
														className="btn waves-effect waves-light btn-add-channel-exclusion light-blue"
														onClick={this.addChannelExclusion.bind(this)}
													>
														Add Channel
													</AddButton>
												</div>
											</div>
										</SettingContent>
									</Setting>

									<Setting className="other-settings">
										<SubHeading>Other Settings</SubHeading>

										<SettingContent>
											<div className="pt-30 row">
												<div class="input-field col s12 m8">
													<select id="max-rating" onChange={this.maxRatingChange.bind(this)}>
														<option value="18+">18+</option>
														<option value="teen">Teen</option>
														<option value="family-friendly">Family Friendly</option>
													</select>
													<label>Highest Stream Rating</label>
												</div>
											</div>

											<div className="pt-30">
												<label>
													<input type="checkbox" id="REHOST-TIMEOUT" onClick={this.showHideLists.bind(this)} />
													<span>Enable Host Rotation?</span>
												</label>
											</div>

											<div className="REHOST-TIMEOUT">
												<small>(In Minutes) Recommended: 120-240 (2-4 hours) *Use increments of 10</small>
												<div className="row">
													<div class="input-field col s12 m8">
														<input
															id="rehost-timeout-val"
															type="number"
															step="10"
															class="validate"
															min="30"
															onChange={this.rehostValueChange.bind(this)}
														/>
														<label for="rehost-timeout-val">Rehost Timeout</label>
													</div>
												</div>
												<div>
													<label>
														<input type="checkbox" id="manual-rehost" onClick={this.rehostManualChecked.bind(this)} />
														<span>Enable Rotation Time on User Manually Hosted?</span>
													</label>
												</div>
											</div>

											<div className="pt-30">
												<label>
													<input type="checkbox" id="OFFLINE-TIMEOUT" onClick={this.showHideLists.bind(this)} />
													<span>Disable hosting for a set time after stream ends?</span>
												</label>

												<div className="OFFLINE-TIMEOUT">
													<small>(In Minutes) Recommended: 5-10 *Use increments of 5</small>
													<div className="row">
														<div class="input-field col s12 m8">
															<input
																id="offline-timeout-val"
																type="number"
																step="5"
																class="validate"
																min="5"
																max="60"
																onChange={this.offlineValueChange.bind(this)}
															/>
															<label for="offline-timeout-val">Offline Timeout</label>
														</div>
													</div>
												</div>
											</div>
										</SettingContent>

										<div className="center-align pt-30">
											<button
												className="waves-effect waves-light btn light-blue darken-2"
												onClick={this.submitForm.bind(this)}
											>
												{this.state.saving ? 'Saving...' : 'Save Settings'}
											</button>
										</div>
									</Setting>
								</MainSettings>
							</FormContent>
						</Form>
					</Page>
				)
		else
			return (
				<div className="page">
					<p>Loading settings...</p>
				</div>
			)
	}
}

const List = (props) => (
	<SortList className={props.class || 'list'}>
		{props.items.map((item, i) => (
			<SortListItem key={item[props.data] || i} data={item[props.data] || i}>
				<span>{props.property !== '' ? item[props.property] : item}</span>

				<RemoveButton className="remove" onClick={props.removeClick}>
					&times;
				</RemoveButton>
			</SortListItem>
		))}
	</SortList>
)
