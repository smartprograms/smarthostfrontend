import styled from 'styled-components'
export const Page = styled.div`padding: 100px 0;`

export const Form = styled.div`
	margin: auto;

	width: 85vw;
	max-width: 1000px;

	background: #26282b;
`

export const Header = styled.div`
	width: 100%;
	margin: 0;
	padding: 0;

	background: linear-gradient(45deg, #90b8f8, #5f85db);
`
export const Heading = styled.h1`
	text-align: center;
	vertical-align: center;
	padding: 15px 0;
`
export const FormContent = styled.div`padding: 0 20px 100px;`

export const Setting = styled.div`padding-top: 30px;`

export const SettingContent = styled.div`
	padding-top: 20px;
	padding-bottom: 20px;
`

export const MainSettings = styled.div`
	padding-top: 20px;
	display: none;
`

export const SorryContainer = styled.div`
	padding-top: 40px;
	text-align: center;
`

export const SorryButton = styled.button`
	display: block;
	max-width: 300px;
	margin: 30px auto;
`
export const RemoveButton = styled.button`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	background-color: transparent;
	color: inherit;
	text-shadow: 0 1px 0 #fff;
	font-size: 1.5rem;
	line-height: 1;
	padding: .5rem 1rem;
	border: none;
`

export const SortList = styled.ul`
	display: -ms-flexbox;
	display: flex;
	-ms-flex-direction: column;
	flex-direction: column;
	padding: 20px;
	position: relative;

	width: 100%;

	@media screen and (min-width: 601px) {
		width: 80%;
	}
`
export const SortListItem = styled.li`
	padding: 20px;
	position: relative;
	display: block;
	border: 1px solid rgba(0, 0, 0, .125);

	span: {
		user-select: none;
	}

	cursor: grab;
`

export const AddButton = styled.button`
	margin-top: 8px;
	min-width: 100%;
`
