import { h, Component } from 'preact'
import { Page } from '../../components/ThemedComponents'
import { Link } from 'preact-router'

export default class Error extends Component {
	componentDidMount () {
		if (this.props.status === 401) {
			setTimeout(() => {
				window.location = process.env.API_URL + '/auth/mixer?state=' + this.props.from
			}, 3000)
		}
	}

	render (props) {
		return (
			<Page className="center-align">
				<h1>ERROR {props.status}</h1>
				{props.status === 401 ? (
					<div>
						Looks like you have been logged out, you will be redirect to the login page... or click the login button at
						the top right.
					</div>
				) : (
					<p>{props.message}</p>
				)}

				<Link href="/">Go Back Home</Link>
			</Page>
		)
	}
}
