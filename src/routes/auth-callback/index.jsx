import { h, Component } from 'preact'
import { logout, login } from '../../services/auth'
import { route } from 'preact-router'

export default class AuthCallback extends Component {
	constructor (props) {
		super(props)

		const qs = props.url ? (props.url.split('?')[1] ? queryJSONIFY(props.url.split('?')[1].split('&')) : null) : null

		const jwt = qs ? qs.jwt : null
		const banned = qs ? qs.banned : false

		console.log('URL:', props.url)
		console.log('JWT:', jwt)
		console.log('Banned:', banned)

		if (jwt) login(jwt)
		else if (banned) route('/banned')
		else logout()
	}

	render () {
		return <div>Loading...</div>
	}
}

function queryJSONIFY (query) {
	const queryString = {}

	query.forEach((query) => {
		let part = query.split('=')

		queryString[part[0]] = decodeURIComponent(part[1] || '')
	})

	return queryString
}
