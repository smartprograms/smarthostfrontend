import { h } from 'preact'
import { Page } from '../../components/ThemedComponents'

const UpdateFail = () => (
	<Page className="all-center">
		<h1>Uh oh!</h1>
		<p>Looks like we failed to update your settings, we are sorry!</p>
		<p>No worries though, we saved your settings locally on your browser so you try again later!</p>
		<p>If logging out and back in does not work please be sure to contact us so we can resolve this for you!</p>
	</Page>
)

export default UpdateFail
