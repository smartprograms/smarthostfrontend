import { h } from 'preact'
import { Page } from '../../components/ThemedComponents'

const UpdateSuccess = () => (
	<Page className="all-center">
		<h1>Success!</h1>
		<p>Your settings have successfully been updated!</p>
		<p>These settings will take affect on the next user we host! If you have any questions feel free to contact us!</p>
	</Page>
)

export default UpdateSuccess
