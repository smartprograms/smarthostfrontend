import { h } from 'preact'
import { Page } from '../../components/ThemedComponents'
import { Link } from 'preact-router'

const LeaveSmartHost = () => (
	<Page className="all-center">
		<h1>Sorry to see you leave :(</h1>
		<p>
			We hate to see you go, if you had any issues please be sure to <Link href="/contact">Contact Us</Link> we would
			love to fix any issues you had!
		</p>
	</Page>
)

export default LeaveSmartHost
