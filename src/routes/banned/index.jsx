import { h, Component } from 'preact'
import { Page } from '../../components/ThemedComponents'
import { Link } from 'preact-router'

export default class Banned extends Component {
	render () {
		return (
			<Page className="center-align">
				<h1>suspended</h1>

				<p>Your account has been suspended for violating our Terms of Agreement</p>
				<br />
				<p>
					Think this is an error? <Link href="/contact">Contact Us!</Link>
				</p>
			</Page>
		)
	}
}
