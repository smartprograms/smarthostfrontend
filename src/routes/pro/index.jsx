import { h, Component } from 'preact'
import { Page } from '../../components/ThemedComponents'
import { getAccessToken, setSubscribeState, isLoggedIn } from '../../services/auth'
import { AddButton } from '../settings/components'

import Error from '../error'

import './index.scss'

const stripe = Stripe(process.env.STRIPE_KEY)
const elements = stripe.elements({
	fonts: [
		{
			cssSrc: 'https://rsms.me/inter/inter-ui.css'
		}
	],
	locale: 'auto'
})

const card = elements.create('card', {
	style: {
		base: {
			color: '#32325D',
			fontWeight: 500,
			fontFamily: 'Inter UI, Open Sans, Segoe UI, sans-serif',
			fontSize: '16px',
			fontSmoothing: 'antialiased',

			'::placeholder': {
				color: '#CFD7DF'
			}
		},
		invalid: {
			color: '#E25950'
		}
	}
})

export default class Pro extends Component {
	constructor () {
		super()

		this.setState({
			loading: true
		})
	}

	handleSubscription (subscription, hideError) {
		try {
			if (!subscription || !(subscription.status === 'active' || subscription.status === 'trialing')) throw 'Not subbed'

			setSubscribeState(true)

			const end = new Date(subscription.current_period_end * 1000).toString()
			this.setState({
				paying: false,
				subscribed: true,
				subscriptionEnd: end,
				canceled: subscription.cancel_at_period_end
			})
		} catch (error) {
			setSubscribeState(false)

			this.setState({
				paying: false,
				subscribed: false
			})

			console.error(error)

			if (hideError) return

			M.toast({
				html: 'Oops, you are not a sub you cannot do that.',
				displayLength: 10000,
				classes: 'red'
			})
		}
	}

	componentDidMount () {
		if (!isLoggedIn()) return this.setState({ error: true })

		this.getSubscriptionPlans()
	}

	getCurrentSubscription () {
		fetch(process.env.API_URL + '/payments/subscription/current', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: getAccessToken()
			}
		})
			.then((res) => {
				if (!res.ok) throw 'Error loading current subscription'

				return res.json()
			})
			.then((res) => {
				this.state.loading = false

				this.handleSubscription(res.subscription, true)
			})
			.catch((err) => {
				setSubscribeState(false)

				this.setState({
					paying: false,
					subscribed: false
				})

				console.error(err)

				M.toast({
					html: 'Oops, there was an error getting you current subscription status.',
					displayLength: 10000,
					classes: 'red'
				})
			})
	}

	getSubscriptionPlans () {
		fetch(process.env.API_URL + '/payments/subscription/plans', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: getAccessToken()
			}
		})
			.then((res) => {
				if (!res.ok) throw 'Error loading plans'

				return res.json()
			})
			.then((res) => {
				console.log(res)

				this.state.plans = res

				this.getCurrentSubscription()
			})
			.catch((err) => {
				console.error(err)

				M.toast({
					html: 'Oops, there was an error getting the subscription plans.',
					displayLength: 10000,
					classes: 'red'
				})
			})
	}

	componentDidUpdate () {
		if (!this.state.subscribed) this.mountCard()
	}

	mountCard () {
		const cardElem = document.getElementById('card')
		if (cardElem) card.mount(cardElem)
	}

	handleForm (e) {
		e.preventDefault()

		if (this.state.paying) return

		this.setState({
			paying: true
		})

		const startSubscription = (token) => {
			const val = document.getElementById('plan-select').value

			const body = JSON.stringify({
				token,
				plan: this.state.plans[val].id,
				code: this.state.codeApplied
			})

			fetch(process.env.API_URL + '/payments/subscription/start', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: getAccessToken()
				},
				body
			})
				.then((res) => {
					if (!res.ok) throw res

					return res.json()
				})
				.then((res) => {
					this.handleSubscription(res.subscription)
				})
				.catch((err) => {
					if (err.ok === false) {
						err
							.json()
							.then((res) => {
								M.toast({
									html: 'Error Subscribing: ' + res.message,
									displayLength: 10000,
									classes: 'red'
								})
							})
							.catch((err) => {
								M.toast({
									html: 'Error Subscribing: Server is having trouble processing request, try again later',
									displayLength: 10000,
									classes: 'red'
								})
							})
					} else {
						M.toast({
							html: 'Error Subscribing: Reason unknown, please contact us',
							displayLength: 10000,
							classes: 'red'
						})
					}

					this.setState({
						paying: false
					})
				})
		}

		const getToken = () => {
			stripe
				.createToken(card)
				.then(({ error, token }) => {
					if (error) throw error

					startSubscription(token.id)
				})
				.catch((err) => {
					console.error(err)

					M.toast({
						html: 'Card Error: ' + err.message,
						displayLength: 10000,
						classes: 'red'
					})

					this.setState({
						paying: false
					})
				})
		}

		getToken()
	}

	cancelSub () {
		fetch(process.env.API_URL + '/payments/subscription/end', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: getAccessToken()
			}
		})
			.then((res) => res.json())
			.then((res) => {
				this.handleSubscription(res.subscription)
			})
			.catch((err) => {
				M.toast({
					html: 'Error, could not cancel the subscription. Try again, or contact us!',
					displayLength: 10000,
					classes: 'red'
				})

				console.error(err)
			})
	}

	getCouponDiscount () {
		const codeElem = document.querySelector('#coupon')
		const code = codeElem.value

		if (!code) {
			M.toast({
				html: 'You must enter a coupon code to use.',
				displayLength: 10000,
				classes: 'red'
			})
			return
		}

		if (this.state.checking) return

		this.setState({ checking: true })

		fetch(process.env.API_URL + '/payments/coupon/' + code, { headers: { Authorization: getAccessToken() } })
			.then((res) => {
				if (res.ok) return res.json()
				else throw res
			})
			.then((response) => {
				if (response.type === 'amount') {
					this.setState({
						couponText: '$' + response.discount / 100 + ' off!',
						couponApplied: true,
						codeApplied: code,
						amount_off: response.discount,
						percent_off: null
					})
				} else if (response.type === 'percent') {
					this.setState({
						couponText: response.discount + '% off!',
						couponApplied: true,
						codeApplied: code,
						percent_off: response.discount,
						amount_off: null
					})
				} else {
					M.toast({
						html: 'Oops an unknown error occurred try again later.',
						displayLength: 10000,
						classes: 'red'
					})
					codeElem.value = ''
				}

				this.setState({ checking: false })
			})
			.catch((err) => {
				err
					.json()
					.then((error) => {
						if (error.statusCode === 404) {
							M.toast({
								html: 'That code is not valid!',
								displayLength: 10000,
								classes: 'red'
							})
						} else {
							M.toast({
								html: 'Oops an unknown error occurred try again later.',
								displayLength: 10000,
								classes: 'red'
							})
						}
					})
					.catch((err) => {
						M.toast({
							html: 'Oops an unknown error occurred try again later.',
							displayLength: 10000,
							classes: 'red'
						})
					})

				codeElem.value = ''
				this.setState({ checking: false })
			})
	}

	render () {
		if (this.state.error) return <Error status={401} message={'Not Authenticated'} from="pro" />
		if (this.state.loading)
			return (
				<Page>
					<div className="all-center">Loading...</div>
				</Page>
			)

		return (
			<Page>
				{!this.state.subscribed ? (
					<div className="form-container">
						<div className="row">
							<ProPerks />
							<CardForm
								handleForm={this.handleForm.bind(this)}
								paying={this.state.paying}
								plans={this.state.plans}
								checking={this.state.checking}
								couponApplied={this.state.couponApplied}
								couponText={this.state.couponText}
								handleCode={this.getCouponDiscount.bind(this)}
								amount_off={this.state.amount_off}
								percent_off={this.state.percent_off}
							/>
						</div>
						<div className="row" style="margin: 0 auto;">
							<ExplainProPerks />
						</div>
					</div>
				) : (
					<div className="all-center">
						<h1 className="heading">Thanks for Subscribing!</h1>
						<h5>You are a PRO!</h5>
						{this.state.canceled ? <p>Sub Ends on:</p> : <p>Sub Renews On:</p>}
						<p>{this.state.subscriptionEnd}</p>

						{this.state.canceled ? (
							<div />
						) : (
							<button className="waves-effect waves-light btn" onClick={this.cancelSub.bind(this)}>
								End Subscription
							</button>
						)}
					</div>
				)}
			</Page>
		)
	}
}

const ProPerks = () => {
	return (
		<div className="col s12 l4">
			<div className="perks all-center">
				<h5>Perks:</h5>
				<ul style="list-style: none;">
					<li>Use More Host Styles</li>
					<li>Larger Host List</li>
					<li>Remove Promo From Host Message</li>
					<li>Get New Features First</li>
					<li>Supporting The Development!</li>
					<li>& More Coming Soon</li>
				</ul>
			</div>
		</div>
	)
}

const ExplainProPerks = () => {
	return (
		<div className="col s12 all-center" style="padding-top: 100px; padding-bottom: 100px;">
			<h4>What exactly is "PRO"?</h4>

			<br />

			<h5 style="font-weight: 700;">Use More Host Styles:</h5>
			<p>You can use more than just 1 Hosting Style, select as many as you want to use!</p>

			<br />

			<h5 style="font-weight: 700;">Larger Host List:</h5>
			<p>Have a lot of people you want to host?</p>
			<p>
				Rather than being limited to 100 Channels if using the "Channels List" host style, you can now host an
				unlimited* amount of users.
			</p>
			<p>
				Rather than being limited to 3 teams if using the "Teams List" host style, you can now host an unlimited* amount
				of teams.
			</p>

			<br />

			<h5 style="font-weight: 700;">Remove Promo From Host Message:</h5>
			<p>Without pro if you use the host message we add on a little promo message, nothing harmful!</p>

			<br />

			<h5 style="font-weight: 700;">Get New Features First:</h5>
			<p>When we release features, you will be the first to be able to use them!</p>

			<br />

			<h5 style="font-weight: 700;">Supporting The Development:</h5>
			<p>Buy purchasing Pro you are supporting our development and the ability to create apps!</p>

			<br />
			<br />

			<small>*Unlimited within reason, we have the right to remove channels/teams if needed.</small>
		</div>
	)
}

class CardForm extends Component {
	constructor () {
		super()

		this.setState({
			selectedPlan: 0
		})
	}

	componentDidMount () {
		document.querySelector('#coupon').addEventListener('keydown', (event) => {
			const keyCode = event.which || event.keyCode
			if (keyCode === 13) document.querySelector('.apply-coupon').click()
		})
	}

	handleSelectValue (e) {
		this.setState({
			selectedPlan: e.target.value
		})
	}

	render (props) {
		return (
			<div className="col s12 l8">
				<div className="plans all-center">
					<h6>Select a plan type</h6>
					<div class="input-field" style="max-width: 450px; margin: 0 auto;">
						<select onChange={this.handleSelectValue.bind(this)} id="plan-select">
							{props.plans.map((plan, index) => {
								return (
									<option value={index}>
										{plan.nickname} (${plan.amount / 100})
									</option>
								)
							})}
						</select>
					</div>
					{<script>M.AutoInit();</script>}
				</div>
				<form action="/charge" method="post" onSubmit={props.handleForm}>
					<div id="paymentRequest" />

					<fieldset>
						<legend class="card-only">Subscribe to Pro</legend>

						<div class="container">
							<div id="card" />
							<button type="submit" data-tid="elements_examples.form.donate_button">
								{props.paying ? (
									'Paying...'
								) : (
									`Subscribe to Pro ${props.amount_off
										? 'First Month: $' +
											(props.plans[this.state.selectedPlan].amount / 100 - props.amount_off / 100).toFixed(2) +
											' then: $' +
											(props.plans[this.state.selectedPlan].amount / 100).toFixed(2)
										: props.percent_off
											? 'First Month: $' +
												(props.plans[this.state.selectedPlan].amount / 100 -
													props.plans[this.state.selectedPlan].amount / 100 * (props.percent_off / 100)).toFixed(2) +
												' then: $' +
												(props.plans[this.state.selectedPlan].amount / 100).toFixed(2)
											: '$' + (props.plans[this.state.selectedPlan].amount / 100).toFixed(2)}/${props.plans[
										this.state.selectedPlan
									].interval_count > 1
										? `${props.plans[this.state.selectedPlan].interval_count} ${props.plans[this.state.selectedPlan]
												.interval}s`
										: `${props.plans[this.state.selectedPlan].interval}`}`
								)}
							</button>
						</div>
					</fieldset>
				</form>

				<div className="coupons" style="padding-top: 10px;">
					<div class="input-field" style="max-width: 450px; margin: 0 auto;">
						<input id="coupon" type="text" style="text-transform:uppercase" />
						<label for="coupon">Coupon Code</label>
					</div>
					<div style="max-width: 450px; margin: 0 auto;">
						<AddButton className="apply-coupon" onClick={props.handleCode}>
							{props.checking ? (
								'Checking Code...'
							) : props.couponApplied ? (
								'Coupon Applied! ' + props.couponText
							) : (
								'Apply Code'
							)}
						</AddButton>
					</div>
				</div>
			</div>
		)
	}
}
