import { h, Component } from 'preact'
import { Page } from '../../components/ThemedComponents'
import { Link } from 'preact-router'

import './index.scss'

export default class HomeNoAuth extends Component {
	componentDidMount () {
		const scrollToElem = document.querySelector('.features')
		document.querySelector('.scroll').addEventListener('click', () => {
			scrollIt(scrollToElem)
		})
	}

	render (props) {
		if (props.url) {
			const query = props.url.split('?')[1]

			if (query) {
				const auth = queryJSONIFY(query.split('&')).auth

				if (auth === 'false') {
					M.toast({ html: 'Oops, there was an error authenticating :(', displayLength: 60000, classes: 'red accent-4' })
				}
			}
		}

		return (
			<Page>
				<div className="main-heading all-center">
					<h1 className="heading">SmartHost</h1>
					<p className="description">
						Mixer's best auto hosting tool that allows you to take control of your hosting even while you are offline.
					</p>
					<a href={process.env.API_URL + '/auth/mixer'}>
						<button className="btn btn-cta">TAKE CONTROL</button>
					</a>

					<div className="scroll">
						<p>See Our Features</p>

						<svg
							xmlns="http://www.w3.org/2000/svg"
							aria-hidden="true"
							focusable="false"
							role="img"
							viewBox="0 0 448 512"
							className="arrow-thing"
						>
							<path d="M413.1 222.5l22.2 22.2c9.4 9.4 9.4 24.6 0 33.9L241 473c-9.4 9.4-24.6 9.4-33.9 0L12.7 278.6c-9.4-9.4-9.4-24.6 0-33.9l22.2-22.2c9.5-9.5 25-9.3 34.3.4L184 343.4V56c0-13.3 10.7-24 24-24h32c13.3 0 24 10.7 24 24v287.4l114.8-120.5c9.3-9.8 24.8-10 34.3-.4z" />
						</svg>
					</div>
				</div>

				<div className="features all-center">
					<h1 className="heading">
						Hosting, <span>Your Way</span>
					</h1>

					<div className="feature-container">
						<div className="feature">
							<h5 className="sub-heading">Hosting Styles</h5>
							<p>
								Host exactly who you want with hosting styles, this is how you select the person you want to host
								whether it be from a channel list you provide, a user on a certain stream team, someone who you follow,
								and more.
							</p>
						</div>
						<div className="feature">
							<h5 className="sub-heading">Host Messaging</h5>
							<p>
								Want to let the person you are hosting know that you have them on your auto-host list? Send them a
								whisper to let them know that they have a special place on your list!
							</p>
						</div>
						<div className="feature">
							<h5 className="sub-heading">Host Rating</h5>
							<p>
								Is your content strictly non-mature content? Use this feature so that you do not host any content rated
								higher than what you allow, keeping your viewers safe!
							</p>
						</div>
						<div className="feature">
							<h5 className="sub-heading">AND MORE!</h5>
							<p>
								And many more features, and more to come. Have a feature in mind that we do not already have? Be sure to{' '}
								<Link href="/contact">contact us</Link> so that we can see about adding it! We love making you happy!
							</p>
						</div>
					</div>

					<div className="cta">
						<a href={process.env.API_URL + '/auth/mixer'}>
							<button className="btn btn-cta">START NOW!</button>
						</a>
					</div>
				</div>
			</Page>
		)
	}
}

function redirect () {
	window.location = process.env.API_URL + '/auth/mixer'
}

function scrollIt (element) {
	window.scrollTo({
		behavior: 'smooth',
		left: 0,
		top: element.offsetTop
	})
}

function queryJSONIFY (query) {
	const queryString = {}

	query.forEach((query) => {
		let part = query.split('=')

		queryString[part[0]] = decodeURIComponent(part[1] || '')
	})

	return queryString
}
