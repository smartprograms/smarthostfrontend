import { h, Component } from 'preact'
import { Page } from '../../components/ThemedComponents'
import { getUser, getAccessToken } from '../../services/auth'
import { route, Link } from 'preact-router'

import Error from '../error'

import './index.scss'

export default class HomeAuth extends Component {
    componentDidMount() {
        fetch(process.env.API_URL + '/users/current/status', {
            headers: {
                Authorization: getAccessToken()
            }
        })
            .then((res) => {
                if (!res.ok) throw res
                return res.json()
            })
            .then((res) => {
                this.setState(res)

                this.getUsername(res.hosting)
            })
            .catch((err) => {
                console.error(err)
                this.setState({ error: 'Could Not Load Current App Status.' })
            })
    }

    getUsername(channelid) {
        fetch('https://mixer.com/api/v1/channels/' + channelid)
            .then((res) => {
                if (!res.ok) throw res
                return res.json()
            })
            .then((res) => {
                this.setState({ hosting: res.token })
            })
            .catch((err) => {
                console.error(err)
            })
    }

    render() {
        if (this.state.error)
            return <Error status="500" message={this.state.error} />
        return (
            <Page>
                <div className="all-center">
                    <h1 className="heading">
                        Welcome{' '}
                        {JSON.parse(getUser()).username ||
                            'I messed up my code but you are logged in...'}
                    </h1>
                    <p className="description">
                        Thanks for choosing SmartHost! Did you know we have a
                        PRO Plan? You should check it out!
                    </p>
                    <br />
                    <p>
                        Want to see us work our magic? Be sure to check out{' '}
                        <a href="https://mixer.com/unsmart">
                            Unsmart on Mixer!
                        </a>
                    </p>
                    <Link href="/settings">
                        <button className="btn btn-cta">EDIT SETTINGS</button>
                    </Link>
                </div>

                <div className="all-center" style="padding-top: 50px;">
                    <p className="heading">Current App Status</p>
                    <p>
                        App is{' '}
                        {this.state.appStatus
                            ? ' running'
                            : 'not running. Do you have it activated? If so, try to log out and back in.'}
                    </p>
                    {this.state.appStatus ? (
                        <div>
                            <p>
                                You are currently:{' '}
                                {typeof this.state.offlineAt === 'number'
                                    ? 'Offline for ' +
                                      this.state.offlineAt +
                                      ' hours'
                                    : 'LIVE'}
                            </p>
                            <p>
                                You are hosting:{' '}
                                {typeof this.state.hosting === 'number' ||
                                typeof this.state.hosting === 'string'
                                    ? 'Hosting ' +
                                      this.state.hosting +
                                      ' for ' +
                                      this.state.hostedAt +
                                      ' minutes'
                                    : 'Not Hosting Anyone'}
                            </p>
                            <p>
                                Next Host Attempt:{' '}
                                {typeof this.state.offlineAt === 'number'
                                    ? 'Every ' +
                                      this.state.hostCheck +
                                      ' minutes'
                                    : this.state.hostCheck +
                                      ' minutes after you go offline'}
                            </p>
                        </div>
                    ) : (
                        <div />
                    )}
                    <div style="max-width: 800px; padding: 20px; margin-top: 100px; margin: 0 auto;">
                        <p>
                            Status not right? Currently the easiest way to fix
                            this is to deactivate the app in the settings -
                            click leave smarthost (it will not delete your
                            account or settings). Then go back and reactivate
                            and save your settings.
                        </p>
                    </div>
                </div>
            </Page>
        )
    }
}
