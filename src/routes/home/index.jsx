import { h, Component } from 'preact'
import { isLoggedIn } from '../../services/auth'
import HomeAuth from './auth'
import HomeNoAuth from './nonauth'
import { Link } from 'preact-router'

export default class Home extends Component {
	render () {
		return <div>{isLoggedIn() ? <HomeAuth /> : <HomeNoAuth />}</div>
	}
}
