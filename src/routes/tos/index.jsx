import { h, Component } from 'preact'
import { Page } from '../../components/ThemedComponents'

export default class TOS extends Component {
	render () {
		return (
			<Page className="center-align" style="max-width: 70vw; margin: 0 auto; text-align: justify; line-height: 1.4;">
				<div className="header" style="padding-bottom: 5px;">
					<div className="row">
						<div className="col s12 l8" style="padding: 0; margin: 0; text-align: left;">
							<h3 style="padding: 0; margin: 0;">SmartHost Terms of Service</h3>
						</div>
						<div className="col s12 l4" style="text-align: right;">
							<p>Updated: October 23, 2019</p>
						</div>
					</div>

					<p>These terms and conditions outline the rules and regulations for the use of SmartHost's Website.</p>
					<p>
						SmartHost is located at: <a href="/">https://smarthostapp.com</a>
					</p>
				</div>

				<p>
					By accessing this website we assume you accept these terms and conditions in full. Do not continue to use
					SmartHost's website if you do not accept all of the terms and conditions stated on this page.
				</p>
				<br />

				<p>
					The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and
					any or all Agreements: "Client", "You" and "Your" refers to you, the person accessing this website and
					accepting the Company's terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our
					Company. "Party", "Parties", or "Us", refers to both the Client and ourselves, or either the Client or
					ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the
					process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed
					duration, or any other means, for the express purpose of meeting the Client's needs in respect of provision of
					the Company's stated services/products, in accordance with and subject to, prevailing law of Texas. Any use of
					the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken
					as interchangeable and therefore as referring to same.
				</p>
				<br />

				<div className="term">
					<h5>Account</h5>
					<p>
						All accounts are provided by <a href="https://mixer.com">Mixer</a> any information given to us about your
						account is governed by the terms and agreements of <a href="https://mixer.com">Mixer</a>. SmartHost does not
						provide account creation.
					</p>
					<p style="padding-top: 3px;">
						Our service provides you with additional extension on your <a href="https://mixer.com">Mixer</a> account. We
						The Company have the right to revoke your account of these additional services, with or without reason.
					</p>
				</div>

				<div className="term">
					<h5>Services</h5>
					<p>
						By accessing SmartHost We The Company provide you additional services on your{' '}
						<a href="https://mixer.com">Mixer</a> account. These services are provided on an "as is" and "as available"
						basis. We The Company have the right to at any time terminate your account, with or without reason.
					</p>
					<p style="padding-top: 3px;">
						Our service contains subscription plans to unlock more features. We The Company have the right to modify,
						change, or revoke these plans from any user, with or without reason. We The Company do not offer refunds for
						purchases, all purchases are final and non-refundable or exchangeable.
					</p>
				</div>

				<div className="term">
					<h5>Payments and Subscriptions</h5>
					<p>
						SmartHost uses <a href="https://stripe.com">Stripe</a> to process all payments and handle all subscriptions.
						Any payment information you provide us are governed by the terms and agreements of{' '}
						<a href="https://stripe.com">Stripe</a>. Any payment you make for the services We The Company provide are
						final and non-refundable or exchangeable.
					</p>
					<p style="padding-top: 3px;">
						Any subscription you make for this service can be revoked without re-payment, with or without reason. Any
						breach of these terms will result in immediate suspension and the immediate revocation of your account and
						any subscriptions on your account, without re-payment.
					</p>
				</div>
			</Page>
		)
	}
}
