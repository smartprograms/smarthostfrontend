import styled from 'styled-components'

export const Page = styled.div`padding-top: 100px;`

export const SubHeading = styled.span`
	text-decoration: underline;
	font-size: 1.3em;

	color: #5f85db;
`
