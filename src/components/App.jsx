import { h, Component } from 'preact'
import Router from 'preact-router'

import Nav from './Nav'
import Footer from './Footer'

import Home from '../routes/home'
import Contact from '../routes/contact'
import TOS from '../routes/tos'

import Banned from '../routes/banned'
import Error from '../routes/error'

import AuthCallback from '../routes/auth-callback'

import Pro from '../routes/pro'

import Settings from '../routes/settings'
import UpdateSuccess from '../routes/settings-update/success'
import UpdateFail from '../routes/settings-update/fail'
import LeaveSmartHost from '../routes/settings-update/leave'

import '../style/index.scss'

export default class App extends Component {
	handleUrl () {
		window.scrollTo(0, 0)
	}

	render () {
		return (
			<div id="app">
				<Nav />
				<div className="content" style="min-height: calc(100vh - 50px);">
					<Router onChange={this.handleUrl}>
						<Home path="/" />
						<Contact path="/contact" />
						<TOS path="/tos" />
						<Pro path="/pro" />
						<Settings path="/settings" />
						<AuthCallback path="/auth/callback" />
						<UpdateSuccess path="/settings/success" />
						<UpdateFail path="/settings/failure" />
						<LeaveSmartHost path="/settings/leave" />
						<Banned path="/banned" />
						<Error
							default
							status="404"
							message="Page was not found. If you think this is an error be sure to contact us!"
						/>
					</Router>
				</div>
				<Footer />
			</div>
		)
	}
}
