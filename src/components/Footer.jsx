import { h, Component } from 'preact'
import { Link } from 'preact-router'

export default class Footer extends Component {
	render () {
		return (
			<footer className="center-align" style="padding: 10px 15px; position: relative; bottom: 0; left: 0; right: 0;">
				<div className="row" style="padding: 0; margin: 0;">
					<div className="col s6">
						<p>
							Made with ❤️ by <a href="https://mixer.com/unsmart">Unsmart!</a>
						</p>
					</div>
					<div className="col s6">
						<Link href="/tos">Terms of Service</Link>
					</div>
				</div>
			</footer>
		)
	}
}
